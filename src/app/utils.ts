import * as _ from 'lodash';

function toCamelCase(node) {
  if (_.isArray(node)) {
    return _.map(node, toCamelCase);
  }

  if (_.isObject(node)) {
    return objectToCamelCase(node);
  }

  return node;
}

function getValue(value) {
  if (_.isObject(value)) {
    return toCamelCase(value);
  }

  return value;
}

function objectToCamelCase(object) {
  const result = {};

  _.forEach(object, (value, key) => {
    result[_.camelCase(key)] = getValue(value);
  });

  return result;
}

export {
  toCamelCase
};
