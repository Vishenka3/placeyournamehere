import { Component, Input } from '@angular/core';

import { environment } from '../../environments/environment';
import MovieModel from '../model/movie.model';
import GenreModel from '../model/genre.model';

@Component({
  selector: 'app-movie-preview',
  templateUrl: './movie-preview.component.html',
  styleUrls: ['./movie-preview.component.scss']
})
export class MoviePreviewComponent {
  @Input() movie: MovieModel;
  @Input() genres: GenreModel;

  constructor() { }

  get posterPath(): string {
    return environment.imageURL + this.movie.posterPath;
  }
}
