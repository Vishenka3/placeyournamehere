import MovieModel from './movie.model';

export default class Movies {
    page: number;
    total_results: number;
    total_pages: number;
    results: MovieModel[];
}
