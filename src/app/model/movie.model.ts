import GenreModel from './genre.model';
import ProductionCountries from './production-countries.model';

export default class MovieModel {
  id: number;
  title: string;
  posterPath: string;
  overview: string;
  voteAverage: number;
  voteCount: number;
  releaseDate: string;
  genreIds: number[] | string[];
  genres: GenreModel[];
  productionCountries: ProductionCountries[];
  budget: number;
  runtime: number;
  tagline: string;
}
