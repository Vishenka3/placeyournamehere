export default class GenreModel {
  id: number;
  name: string;

  constructor(obj: {id: number; name: string}) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}
