import { Component, OnInit } from '@angular/core';
import { faAngleUp } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-scroll-to-top',
  templateUrl: './scroll-to-top.component.html',
  styleUrls: ['./scroll-to-top.component.scss']
})
export class ScrollToTopComponent implements OnInit {
  isScrollButtonEnabled = false;
  faAngleUp = faAngleUp;

  constructor() { }

  scrollToTop(): void {
    window.scrollTo(0, 0);
  }

  ngOnInit() {
    window.onscroll = () => {
      this.isScrollButtonEnabled = window.pageYOffset > 2500;
    };

  }

}
