import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { combineLatest } from 'rxjs';

import MovieModel from '../model/movie.model';
import GenreModel from '../model/genre.model';

import { MoviesService } from '../services/movies/movies.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-movies-list',
  templateUrl: './main-movies-list.component.html',
  styleUrls: ['./main-movies-list.component.scss']
})
export class MainMoviesListComponent implements OnInit {
  movies: MovieModel[];
  genres: GenreModel[];
  totalPages: number;
  currentPage = 1;

  constructor(
    private moviesService: MoviesService,
    private location: Location,
    route: ActivatedRoute
  ) {
    this.currentPage = route.snapshot.queryParams.page || 1;
    this.nextPage = this.nextPage.bind(this);
    this.prevPage = this.prevPage.bind(this);
    this.changePage = this.changePage.bind(this);
  }

  parseGenres(genresArray): string {
    let arr: any[];
    arr = this.genres ? this.genres.filter((item: GenreModel) => {
      return genresArray.includes(item.id);
    }) : [];

    return arr.map( item => item.name).join(', ');
  }

  changePage(event): void {
    this.currentPage = event.target.value;
    this.location.go(`/main?page=${this.currentPage}`);
    this.getMovies();
  }

  nextPage(): void {
    this.currentPage++;
    this.location.go(`/main?page=${this.currentPage}`);
    this.getMovies();
  }

  prevPage(): void {
    this.currentPage--;
    this.location.go(`/main?page=${this.currentPage}`);
    this.getMovies();
  }

  getMovies(): void {
    combineLatest(this.moviesService.getGenres(), this.moviesService.getMovies(this.currentPage))
      .subscribe( ([genres, movies]) => {
        this.movies = movies;
        this.totalPages = environment.totalPages;
        this.genres = genres;
    });
  }

  ngOnInit() {
    this.getMovies();
  }
}
