import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import MovieModel from '../model/movie.model';
import GenreModel from '../model/genre.model';
import ProductionCountries from '../model/production-countries.model';

import { MoviesService } from '../services/movies/movies.service';

import { environment } from '../../environments/environment';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {
  movie: MovieModel;
  genres: string;
  currentMovieId: number;
  private sub: any;

  constructor(
    private moviesService: MoviesService,
    private location: Location,
    private route: ActivatedRoute
  ) {
    this.currentMovieId = route.snapshot.params.id;
  }

  get posterPath(): string {
    return environment.imageURL + this.movie.posterPath;
  }

  parseGenres(): string {
    return this.movie.genres.map((item: GenreModel) => item.name).join(', ');
  }

  parseProductionCountries(): string {
    return this.movie.productionCountries.map((item: ProductionCountries) => item.name).join(', ');
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.currentMovieId = +params['id'];
    });

    this.moviesService.getMovieById(this.currentMovieId)
      .subscribe( data => {
        console.log(data);
        this.movie = data;
        this.parseGenres();
      });
  }
}
