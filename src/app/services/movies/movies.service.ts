import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { toCamelCase } from '../../utils';

import MovieModel from '../../model/movie.model';
import GenreModel from '../../model/genre.model';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  movies: MovieModel;

  constructor(private http: HttpClient) { }

  getMovieById(id: number): Observable<MovieModel> {
    return this.http.get<MovieModel>(`movie/${id}`, {params: {language: 'en-US'}})
      .pipe(map( (item: any) => toCamelCase(item)));
  }

  getMovies(currentPage: number): Observable<MovieModel[]> {
    return this.http.get<MovieModel>(`discover/movie`, {
      params: {
        page: currentPage.toString(),
        language: 'en-US',
        sort_by: 'popularity.desc'
      }
    }).pipe(map( (item: any) => {
      environment.totalPages = item.total_pages;
      return item.results.map( object => toCamelCase(object));
    }));
  }

  getGenres(): Observable<GenreModel[]> {
    return this.http.get<GenreModel>(`genre/movie/list`, {params: {language: 'en-US'}}).pipe(map( (item: any) =>
      item.genres.map( i => new GenreModel(i))
    ));
  }
}
