import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule , HTTP_INTERCEPTORS } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { MainMoviesListComponent } from './movies-list/main-movies-list.component';
import { MoviePreviewComponent } from './movie-preview/movie-preview.component';
import { QueryInterceptor } from './query.interceptor';
import { PaginationComponent } from './pagination/pagination.component';
import { ScrollToTopComponent } from './scroll-to-top/scroll-to-top.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainMoviesListComponent,
    MoviePreviewComponent,
    PaginationComponent,
    ScrollToTopComponent,
    MovieDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: QueryInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
